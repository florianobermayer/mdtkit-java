
/*
  A comment is an inverse definition of other existing data constructs.
*/
Comment "comment"
 = !(TestTitle / Codeblock / Table) CommentText !(TestTitle / Codeblock / Table)
 {
   return buildValue(text, Type.COMMENT, location, path);
 }

/*
  The CommentText can consist of any char sequence (besides newline)
*/
CommentText
 = (!NEWLINE .)*
