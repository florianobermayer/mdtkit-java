NotProperlyFormattedStringInTableTest
-------------------------------------

This test tests the behavior of mal-formatted strings in data tables:

It expects a `MDParserException("string not properly formatted: [I am mal-formatted because I'm not surrounded by matching quotes]")`

| malformattedString:string                                        |
|:-----------------------------------------------------------------|
| I am mal-formatted because I'm not surrounded by matching quotes |
