UnsupportedTypeInConstTest
--------------------------

This test tests handling of unsupported types in pre or post constants:

It expects a `MDParserException("unsupported type: 'unsupported_type'")` Exception.

```
pre var:unsupported_type = 0;
```

| var:number |
|:----------:|
|     0      |
