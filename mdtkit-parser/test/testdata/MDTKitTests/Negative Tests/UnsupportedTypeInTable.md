UnsupportedTypeInTableTest
--------------------------

This test tests handling of unsupported types in tables:

It expects a `MDParserException("unsupported type: 'unsupported_type'")` Exception.

| var:unsupported_type |
|:--------------------:|
|          0           |
