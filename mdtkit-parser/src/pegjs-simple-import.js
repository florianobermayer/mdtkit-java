/**
 * Created by Florian on 10.11.2015.
 */
'use strict';

const fs = require("fs");
const path = require("path");
const XRegExp = require("xregexp");

const ImportRegex = /\/\/[\t ]*@append[\t ]+"(.*\.pegjs)"/g;

class SimpleImporter {

    static GetContentWithResolvedDependencies(filepath) {

        // Read file content
        const content = fs.readFileSync(filepath).toString();


        // Get imports

        const importsResolved = SimpleImporter.GetImportsList(content, filepath);

        // Replace imports
        var res = content;

        importsResolved.forEach(function (elem) {
            if (elem["ml_comment"]) {
                throw new Error("not implemented!") // TODO: support ml comments
                const ml_regex = XRegExp('\\/\\/?\\*.*?@append[\\t\\s]+"' + elem["relative_path"] + '"[.\\s]*\\*\\/', "g")
                res = res.replace(ml_regex, elem["content"] + "\n")
            } else {
                const sl_regex = XRegExp('\\/\\/[\\t ]*@append[\\t ]+"' + elem["relative_path"] + '"', "g");
                res = res.replace(sl_regex, elem["content"] + "\n");

            }
        });
        return res;
    }

    static GetImportsList(content, filepath) {
        const importsDefinitions = SimpleImporter.GetImportDefinitions(content);
        return SimpleImporter.GetImportList(importsDefinitions, filepath);
    }

    static GetImportDefinitions(content) {
        const result = [];

        var m;
        while ((m = ImportRegex.exec(content)) !== null) {
            if (m.index === ImportRegex.lastIndex) {
                ImportRegex.lastIndex++;
            }

            for (var i = 1; i < m.length; i++) {
                if (m[i] === undefined) {
                    continue;
                }

                result.push({"relative_path": m[i].toString(), "ml_comment": false}); // TODO: change when ml comment is supported
            }
        }
        return result;
    }

    static GetImportList(importDefinitions, filepath) {
        const result = [];

        importDefinitions.forEach(function (definition) {
            const absolutePath = path.join(path.dirname(filepath), definition["relative_path"]);
            const content = SimpleImporter.GetContentWithResolvedDependencies(absolutePath);
            result.push({
                "relative_path": definition["relative_path"],
                "content": content,
                "ml_comment": definition["ml_comment"]
            });
        });

        return result;
    }
}

module.exports = SimpleImporter;
module.exports.AppendInstruction = "@append";




