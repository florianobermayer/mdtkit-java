'use strict';
const Utils = require("./utils");
const Type = require("./literal-type");
const _ = require("lodash");

class SimpleArithmeticsResolver{

    constructor(arithmeticParser){
        this._parser = arithmeticParser;
    }

    resolve(parserResult, errorManager){

        _.each(parserResult.testCases, testCase => {
            const values = Utils.getValuesOfType(parserResult, testCase.title, [Type.UNTYPED]);

            const resolveInner = (valueArray, isPrePostArray) => {

                _.each(valueArray, elem =>{

                    const toBeEvaluated = SimpleArithmeticsResolver._resolveLogicalOperators(isPrePostArray? elem.expression.value : elem.value);

                    try{
                        let evaluated = eval(toBeEvaluated);

                        if(elem.validType === "boolean"){
                            evaluated = !!evaluated; // ensures truthy and falthy values
                        }

                        if(!_.isString(evaluated)){
                            evaluated = "" + evaluated;
                        }else{
                            evaluated = "\"" + _.escapeRegExp(evaluated) + "\"";
                        }

                        const res = this._parser.parse(evaluated);

                        if(!Utils.isCorrectType(elem.validType, res.type)){
                            errorManager.raise("Resolved expression '" + res.value + "' has wrong type.\n" +
                                "Required: '" + elem.validType + "'\t Actual: '" + res.type.validTypeName + "'.",
                                isPrePostArray ? elem.expression.location : elem.location, isPrePostArray ? elem.expression.path : elem.path);
                        }
                        if(isPrePostArray){
                            elem.expression.type = res.type;
                            elem.expression.value = res.value;
                        }else{
                            elem.type = res.type;
                            elem.value = res.value;
                        }
                        elem.arithmeticallyResolved = true;
                    }catch (e){
                        errorManager.raise("Could not resolve arithmetic expression: '" + toBeEvaluated + "':\n" + e,
                            isPrePostArray ? elem.expression.location : elem.location, isPrePostArray ? elem.expression.path : elem.path);
                    }
                });
            };

            //calculate pre / post values
            resolveInner(values.prePostConstants, true);

            // calculate table values
            resolveInner(values.tableValues, false);
        });

        return parserResult;
    }

    /**
     *
     * @param {String} inputStr
     * @returns {String}
     * @private
     */
    static _resolveLogicalOperators(inputStr){
        inputStr = Utils.replaceAll(inputStr, " L_AND ", " & ");
        inputStr = Utils.replaceAll(inputStr, " L_OR ", " | ");
        inputStr = Utils.replaceAll(inputStr, " AND ", " && ");
        inputStr = Utils.replaceAll(inputStr, " OR ", " || ");
        return inputStr;
    }
}

module.exports = SimpleArithmeticsResolver;