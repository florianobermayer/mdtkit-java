This test expects an MDParserException("Cannot find 'ThisTestMethodIsNotFoundInTheTestFile' in file 'TestMethodNotInMarkdownTestFileTest.md'!").

Therefore the test method impl. must have a test method named "ThisTestMethodIsNotFoundInTheTestFile"

*Do not* impl the below test method (name):

AnUnusedTestCaseDeclaration
---------------------------

| a:number |
|:---------|
| 0        |
