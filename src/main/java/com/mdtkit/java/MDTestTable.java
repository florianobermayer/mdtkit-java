package com.mdtkit.java;

import com.mdtkit.java.util.MDMap;

import java.util.List;

public class MDTestTable {

    private List<String> headings;
    private List<MDMap> rows;

    public MDTestTable(List<String> headings, List<MDMap> rows) {
        this.headings = headings;
        this.rows = rows;
    }

    public List<String> getHeadings() {
        return headings;
    }

    public List<MDMap> getRows() {
        return rows;
    }

}
