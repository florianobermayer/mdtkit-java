package com.mdtkit.java;

import com.mdtkit.java.util.AggregateException;
import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.util.MDMap;
import com.mdtkit.java.MDExceptions.MDParserException;

public abstract class MDTestCase {

    /**
     * Starts execution of test run. Uses calling method name (which ideally is the test name) automatically.
     *
     * @param runner
     * @throws MDParserException
     * @throws MDLinterException
     * @throws AggregateException
     */
    public void execute(MDTestCaseRunner runner) throws MDParserException, MDLinterException, AggregateException {
        String callingMethod = Thread.currentThread().getStackTrace()[2].getMethodName();
        execute(callingMethod, runner);
    }

    /**
     * Starts execution of test run. Uses class name automatically.
     *
     * @param testName
     * @param runner
     * @throws MDParserException
     * @throws MDLinterException
     * @throws AggregateException
     */
    public void execute(String testName, MDTestCaseRunner runner) throws MDParserException, MDLinterException, AggregateException {
        execute(testName, this.getClass().getSimpleName(), runner);
    }

    /**
     * Starts execution of test run.
     *
     * @param testFilename
     * @param testName
     * @param runner
     * @throws MDParserException
     * @throws MDLinterException
     * @throws AggregateException
     */
    public void execute(String testName, String testFilename, MDTestCaseRunner runner) throws MDParserException, MDLinterException, AggregateException {
        MDTestData testData = MDTestData.create(testFilename, testName);
        innerExecute(testData, runner);

    }

    /**
     * Starts execution of test run with testName and testClass.
     * @param testName
     * @param testClass
     * @param runner
     * @throws MDParserException
     * @throws MDLinterException
     * @throws AggregateException
     */
    public void execute(String testName, Class testClass, MDTestCaseRunner runner) throws MDParserException, MDLinterException, AggregateException {
        MDTestData testData = MDTestData.create(testName, testClass);
        innerExecute(testData, runner);

    }

    private void innerExecute(MDTestData testData, MDTestCaseRunner runner){
        MDTestTable table = testData.getTestTable();
        for (int rowIndex = 0; rowIndex < table.getRows().size(); rowIndex++) {
            MDMap row = table.getRows().get(rowIndex);
            try {
                runner.runOnEachRow(row, rowIndex, testData.getPreConstants(), testData.getPostConstants());
            } catch (AssertionError error) {
                AssertionError newError = new AssertionError("rowIndex " + rowIndex + "\n" + (error.getMessage() != null ? error.getMessage() : ""));
                newError.setStackTrace(error.getStackTrace());
                throw newError;
            }
        }
    }


    public void setTestDataBasePath(String testDataBasePath) {
        MDTestData.setTestDataBasePath(testDataBasePath);
    }
}
