package com.mdtkit.java;

import com.mdtkit.java.util.MDMap;

import org.junit.Assert;

public abstract class MDTestCaseRunner extends Assert {
    public abstract void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants);
}

