package com.mdtkit.java.MDTestRunner;

import com.mdtkit.java.MDExceptions.MDTestSetupException;
import com.mdtkit.java.util.MDMap;
import org.junit.Assert;

abstract class MDTestCaseSetupRunner extends Assert {
    public abstract void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) throws MDTestSetupException;
}

