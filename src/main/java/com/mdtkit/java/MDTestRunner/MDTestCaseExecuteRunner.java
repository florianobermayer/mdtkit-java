package com.mdtkit.java.MDTestRunner;

import com.mdtkit.java.MDExceptions.MDTestExcecutionException;
import com.mdtkit.java.util.MDMap;
import org.junit.Assert;

public abstract class MDTestCaseExecuteRunner extends Assert {
    public abstract void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) throws MDTestExcecutionException;
}