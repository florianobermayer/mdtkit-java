package com.mdtkit.java.MDTestRunner;

import com.mdtkit.java.MDExceptions.MDTestTearDownException;
import com.mdtkit.java.util.MDMap;
import org.junit.Assert;

public abstract class MDTestCaseTearDownRunner extends Assert {
    public abstract void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) throws MDTestTearDownException;
}