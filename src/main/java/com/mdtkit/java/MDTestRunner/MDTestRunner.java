package com.mdtkit.java.MDTestRunner;

import com.mdtkit.java.MDExceptions.*;
import com.mdtkit.java.MDTestCaseRunner;
import com.mdtkit.java.MDTestData;
import com.mdtkit.java.util.*;

import java.util.ArrayList;
import java.util.List;

public class MDTestRunner
{
    public static void Run(
            MDTestCaseExecuteRunner execute,
            boolean failImmediatelyWhenTestRowFails, boolean verbose,
            String callingClassName, String callingMethodName) throws MDParserException, MDLinterException, AggregateException, AbstractMDTestException {
        Run(null, execute, null, failImmediatelyWhenTestRowFails, verbose, callingClassName, callingMethodName);
    }

    public static void Run(
            MDTestCaseSetupRunner setup,
            MDTestCaseExecuteRunner execute,
            MDTestCaseTearDownRunner teardown,
            boolean failImmediatelyWhenTestRowFails, boolean verbose,
            String callingClassName, String callingMethodName) throws MDParserException, MDLinterException, AggregateException, AbstractMDTestException {
        if (callingClassName.endsWith(".java"))
        {
            callingClassName = FileHelper.getFilenameWithoutExtension(callingClassName);
        }

        MDTestData mdTestData = MDTestData.create(callingClassName, callingMethodName);

        if (failImmediatelyWhenTestRowFails)
        {
            // fail test immediately after one MDT Test Row failes.
            // run test with mdtestdata
            for (int i = 0; i < mdTestData.getTestTable().getRows().size(); i++)
            {
                RunTestSequence(mdTestData.getTestTable().getRows().get(i), i, mdTestData.getPreConstants(), mdTestData.getPostConstants(),
                        setup, execute, teardown, verbose);
            }
        }
        else
        {
            // continue rest of lines and fail after whole table if any row failed.
            List<Exception> throwExceptions = new ArrayList<Exception>();
            // run test with mdtestdata
            for (int i = 0; i < mdTestData.getTestTable().getRows().size(); i++)
            {
                try
                {
                    RunTestSequence(mdTestData.getTestTable().getRows().get(i), i, mdTestData.getPreConstants(),
                            mdTestData.getPostConstants(), setup, execute, teardown, verbose);
                }
                catch (Exception e)
                {
                    throwExceptions.add(e);
                }
            }

            if (throwExceptions.size() > 0)
            {
                String separatorLine =
                        "\n\n" + new String(new char[80]).replace("\0", "#") + "\n\n";
                String exMsg = "One or more Exceptions occurred:\n\n\n";

                for (Exception e: throwExceptions) {
                    exMsg += e.getMessage();
                    exMsg += separatorLine;

                }
                exMsg += separatorLine;
                exMsg += "First Exception's Stacktrace:\n\n\n";
                throw new AggregateException(exMsg, throwExceptions);
            }
        }
    }

    private static void RunTestSequence(MDMap row, int index, MDMap pre,
                                        MDMap post,
                                        MDTestCaseSetupRunner setup,
                                        MDTestCaseExecuteRunner execute,
                                        MDTestCaseTearDownRunner teardown,
                                        boolean verbose) throws AbstractMDTestException, AggregateException {
        Exception testException = null;
        try
        {
            if (setup != null)
            {
                if (verbose)
                {
                    System.out.println("--- --- --- --- Setup line [" + index + "] ... --- --- --- ---");
                }
                try
                {
                    setup.runOnEachRow(row, index, pre, post);
                }
                catch (MDTestSetupException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw new MDTestSetupException(e, index);
                }
            }

            if (verbose)
            {
                System.out.println("\n--- --- --- --- run line [" + index + "] ... --- --- --- ---");
            }
            execute.runOnEachRow(row, index, pre, post);
        }
        catch (MDTestSetupException e)
        {
            testException = e;
        }
        catch (Exception e)
        {
            testException = new MDTestExcecutionException(e, index);
        }

        if (teardown != null)
        {
            if (verbose)
            {
                System.out.println("\n--- --- --- --- TearDown line [" + index + "] ... --- --- --- ---");
            }
            try
            {
                teardown.runOnEachRow(row, index, pre, post);
            }
            catch (Exception e)
            {
                if (testException == null)
                {
                    throw new MDTestTearDownException(e, index);
                }
                if (testException.getClass().isInstance(MDTestSetupException.class))
                {
                    throw new AggregateException("SETUP AND TEAR DOWN FAILED!", testException, e);
                }
                throw new AggregateException("TEST AND TEAR DOWN FAILED!", testException,
                        new MDTestTearDownException(e, index));
            }
        }

        if (testException != null)
        {
            throw new RuntimeException(testException);
        }
    }
}

