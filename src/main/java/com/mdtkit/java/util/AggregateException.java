package com.mdtkit.java.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AggregateException extends Exception {

    private final List<Exception> secondaryExceptions;

    public AggregateException(String message, Exception... others) {
        this(message, Arrays.asList(others));
    }

    public AggregateException(Exception... others) {
        this("", others);
    }

    public AggregateException(List<Exception> exceptions) {
        this("", exceptions);
    }

    public AggregateException(String message, List<Exception> exceptions) {
        super(message, exceptions.size() > 0 ? exceptions.get(0) : null);
        this.secondaryExceptions = new ArrayList<Exception>();
        if (exceptions == null) {
            return;
        }
        secondaryExceptions.addAll(exceptions.subList(1, exceptions.size()));
    }

    public List<Throwable> getAllExceptions() {

        List<Throwable> result = new ArrayList<Throwable>();
        result.addAll(getExceptionsRecursive(this));

        for (Throwable t : secondaryExceptions) {
            result.addAll(getExceptionsRecursive(t));
        }
        return result;
    }

    private List<Throwable> getExceptionsRecursive(Throwable t) {
        List<Throwable> result = new ArrayList<Throwable>();
        result.add(t);

        if (t.getCause() != null) {
            result.addAll(getExceptionsRecursive(t.getCause()));
        }
        return result;
    }
}