package com.mdtkit.java.util;

import java.util.Map;

/**
 * Created by Florian on 01.06.2016.
 */
public class Location {
    private final LocationEntry end;
    private final LocationEntry start;

    public Location(LocationEntry start, LocationEntry end) {
        this.start = start;
        this.end = end;
    }

    public static Location parse(Object locationObject) {
        return parse((Map<String, Map<String, Integer>>) locationObject);
    }

    public static Location parse(Map<String, Map<String, Integer>> locationMap) {

        return new Location
                (
                        new LocationEntry(
                                locationMap.get("start").get("column"),
                                locationMap.get("start").get("line"),
                                locationMap.get("start").get("offset")
                        ),
                        new LocationEntry(
                                locationMap.get("end").get("column"),
                                locationMap.get("end").get("line"),
                                locationMap.get("end").get("offset")
                        )
                );

    }

    public LocationEntry getStart() {
        return start;
    }

    public LocationEntry getEnd() {
        return end;
    }

    public String toString() {
        try {
            return new JsonParser().writeAsString(this);
        } catch (ParserException ignored) {
        }
        return super.toString();
    }

    static class LocationEntry
    {
        public LocationEntry(int column, int line, int offset)
        {
            Column = column;
            Line = line;
            Offset = offset;
        }

        public int Column;
        public int Line;
        public int Offset;
    }
}

