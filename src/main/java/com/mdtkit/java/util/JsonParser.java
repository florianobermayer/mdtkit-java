package com.mdtkit.java.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonParser{


    protected ObjectMapper mapper;

    public JsonParser() {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    public <T> T read(String stringToParse, Class<T> type) throws ParserException {
        try {
            T result = mapper.readValue(stringToParse, type);
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public String writeAsString(Object object) throws ParserException {
        try {
            String result = mapper.writeValueAsString(object);
            return result;
        } catch (JsonProcessingException e) {
            throw new ParserException();
        }
    }

    public <T> String writeListAsString(Object object, Class<T> type) throws ParserException {
        try {
            String result = mapper.writerWithType(new TypeReference<List<T>>() {
            }).writeValueAsString(object);
            return result;
        } catch (JsonProcessingException e) {
            throw new ParserException();
        }
    }

    public <T> List<T> readList(String stringToParse, Class<T> type) throws ParserException {
        try {
            List<T> result = mapper.readValue(stringToParse, TypeFactory.defaultInstance().constructCollectionType(ArrayList.class, type));
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public Map<String, Object> readDictionary(File fileToParse) throws ParserException {
        try {
            Map<String, Object> result = mapper.readValue(fileToParse, new TypeReference<Map<String, Object>>() {
            });
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public Map<String, Object> readDictionary(String stringToParse) throws ParserException {
        try {
            Map<String, Object> result = mapper.readValue(stringToParse, new TypeReference<Map<String, Object>>() {
            });
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public Map<String, Object> readDictionary(InputStream streamToParse) throws ParserException {
        try {
            Map<String, Object> result = mapper.readValue(streamToParse, new TypeReference<Map<String, Object>>() {
            });
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public <T> T read(File fileToParse, Class<T> type) throws ParserException {
        try {
            T result = mapper.readValue(fileToParse, type);
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public void write(File file, Object object) throws ParserException {
        try {
            mapper.writeValue(file, object);
        } catch (JsonProcessingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public <T> T read(byte[] bytesToParse, Class<T> type) throws ParserException {
        try {
            T result = mapper.readValue(bytesToParse, type);
            return result;
        } catch (JsonParseException e) {
            throw new ParserException();
        } catch (JsonMappingException e) {
            throw new ParserException();
        } catch (IOException e) {
            throw new ParserException();
        }
    }

    public byte[] writeAsBytes(Object object) throws ParserException {
        try {
            byte[] result = mapper.writeValueAsBytes(object);
            return result;
        } catch (JsonProcessingException e) {
            throw new ParserException();
        }
    }

}
