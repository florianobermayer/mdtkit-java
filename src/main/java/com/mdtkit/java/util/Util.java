package com.mdtkit.java.util;

import java.util.Arrays;
import java.util.List;

public class Util {

    public static String Join(String separator, Object... elems){
        return Join(separator, Arrays.asList(elems));

    }

    public static String Join(String separator, List elems){
        StringBuilder sb = new StringBuilder();
        String delim = "";
        for (Object o : elems) {
            sb.append(delim).append(o);
            delim = separator;
        }
        return sb.toString();
    }

    public static boolean isSameOrSubclass(Class potentialBase, Class potentialDescendant)
    {
        return potentialDescendant.getSuperclass().equals(potentialBase)
                || potentialDescendant == potentialBase;
    }
}
