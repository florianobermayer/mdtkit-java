package com.mdtkit.java.util;

import java.util.Date;
import java.util.HashMap;

public class MDMap extends HashMap<String, Object> {

    public int getInt(String key) {
        return (Integer) super.get(key);
    }

    public double getDouble(String key) {
        return (Double) super.get(key);
    }

    public boolean getBoolean(String key) {
        return (Boolean) super.get(key);
    }

    public Date getDate(String key) {
        return (Date) super.get(key);
    }

    public String getString(String key) {
        return (String) super.get(key);
    }
}
