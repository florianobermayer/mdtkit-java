package com.mdtkit.java.util;

import com.mdtkit.java.MDExceptions.MDParserException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by mcflo_000 on 22.06.2016.
 */
public class PropertyLoader {
    public static final String ConfigFilePath = "config.properties";

    private static final String NODE_PROPERTY_KEY = "node-exec-path";
    private static final String NPM_PROPERTY_KEY = "npm-exec-path";
    private static final String MDTKIT_PROPERTY_KEY = "mdtkit-parser-dir-path";

    private static Properties properties;

    public static String getNodePath() throws MDParserException {
        String result = getProperties(ConfigFilePath).getProperty(NODE_PROPERTY_KEY, null);
        if(new File(result).exists()){
            return result;
        }
        throw new MDParserException("'" + NODE_PROPERTY_KEY + "' not specified properly in " + ConfigFilePath + ". Please add the (absolute) exec path of node to " + ConfigFilePath + "." );
    }

    public static String getNPMPath() throws MDParserException {
        String result = getProperties(ConfigFilePath).getProperty(NPM_PROPERTY_KEY, null);
        if(new File(result).exists()){
            return result;
        }
        throw new MDParserException("'" + NPM_PROPERTY_KEY + "' not specified properly in " + ConfigFilePath + ". Please add the (absolute) exec path of npm to " + ConfigFilePath + "." );
    }

    public static String getMDTKitModuleWorkingDir() throws MDParserException {
        String result =  getProperties(ConfigFilePath).getProperty(MDTKIT_PROPERTY_KEY, null);
        String filePath = getAbsoluteCanonicalPath(result);
        if(filePath != null){
            return filePath;
        }
        throw new MDParserException("'" + MDTKIT_PROPERTY_KEY + "' not specified properly in " + ConfigFilePath + ". Please add the (relative) path to the mdtkit-parser node module directory to " + ConfigFilePath + "." );
    }

    private static String getAbsoluteCanonicalPath(String result) {
        if(result != null){
            try {
                boolean isAbsolute = new File(result).isAbsolute();
                File file = new File(isAbsolute ? result: Util.Join(File.separator, FileHelper.getModulePath(), result)).getCanonicalFile();
                if(file.exists()){
                    return file.getCanonicalPath();
                }
            } catch (IOException ignored) {
            }
        }
        return null;
    }

    private static Properties getProperties(String filePath){
        if(PropertyLoader.properties != null) {
            return PropertyLoader.properties;
        }

        FileReader reader = null;
        PropertyLoader.properties = new Properties();
        try {
            reader = new FileReader(new File(filePath));

            properties.load(reader);

        } catch (FileNotFoundException ignored) {
        } catch (IOException ignored) {
        }finally {
            if(reader != null){
                try{
                    reader.close();
                }catch (Exception ignored){}
            }
        }
        return PropertyLoader.properties;
    }
}
