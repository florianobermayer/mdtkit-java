package com.mdtkit.java.util;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import java.util.regex.Pattern;


public class FileHelper {

    public static String PATH_UP = ".." + File.separator;

    public static boolean deleteFileAndParentIfEmpty(String filePath) {
        if (filePath != null && filePath.length() > 0 && !filePath.equals("")) {
            File file = new File(filePath);
            return deleteFileAndParentIfEmpty(file);
        }

        return true;
    }


    public static List<File> getFileSystemEntries(final File folder, Pattern searchPattern) {
        return getFileSystemEntries(folder, true, searchPattern);
    }

    public static List<File> getFileSystemEntries(final File folder) {
        return getFileSystemEntries(folder, true, null);
    }

    public static List<File> getFileSystemEntries(final File folder, boolean excludeDirectories, Pattern searchPattern) {
        List<File> result = new Vector<File>();
        File [] fileList = folder.listFiles();
        if(fileList == null){
            fileList = new File[0];
        }

        for (final File fileEntry : fileList) {
            if (fileEntry.isDirectory()) {
                if (!excludeDirectories && (searchPattern == null || searchPattern.matcher(fileEntry.getAbsolutePath()).find())) {
                    result.add(fileEntry);
                }
                result.addAll(getFileSystemEntries(fileEntry, excludeDirectories, searchPattern));
            } else {
                if (searchPattern == null || searchPattern.matcher(fileEntry.getAbsolutePath()).find()) {
                    result.add(fileEntry);
                }
            }
        }

        return result;
    }

    public static boolean deleteFileAndParentIfEmpty(File file) {
        if (file == null) {
            return true;
        }

        File parentFile = file.getParentFile();

        if (parentFile.exists()) {
            File[] children = parentFile.listFiles();
            if (children == null || children.length == 0 || (children.length == 1 && children[0].equals(file))) {
                return deleteFolder(parentFile);
            }
        }

        if (file.exists()) {
            boolean deleted = file.delete();
            if (!deleted) {
                return false;
            }
        }

        return true;
    }

    public static boolean deleteFolder(File folder) {
        if (folder == null || !folder.exists()) {
            return true;
        }

        if (folder.isDirectory() && folder.listFiles() != null) {
            for (File f : folder.listFiles()) {
                FileHelper.deleteFolder(f);
            }
        }

        return folder.delete();
    }

    public static boolean createNewFile(File file) {
        if (file == null) {
            return false;
        }

        if (file.exists()) {
            boolean deleted = file.delete();
            if (!deleted) {
                return false;
            }
        }

        if (!file.getParentFile().exists()) {
            boolean created = file.getParentFile().mkdirs();
            if (!created) {
                return false;
            }
        }

        try {
            return file.createNewFile();
        } catch (IOException e) {
            return false;
        } catch (SecurityException e) {
            return false;
        }
    }

    public static String getHumanFileSize(long size) {
        if (size == 1) {
            return size + " Byte";
        }
        if (size < 1024) {
            return size + " Bytes";
        }

        int exp = (int) (Math.log(size) / Math.log(1024));

        char prefix = "KMGTPE".charAt(exp - 1);
        return String.format(Locale.getDefault(), "%.2f %sB", size / Math.pow(1024, exp), prefix);
    }

    public static String getExtension(String filename) {
        int index = filename.lastIndexOf(".");

        if (index > 0) {
            return filename.substring(index + 1, filename.length()).toLowerCase(Locale.ENGLISH);
        } else {
            return "";
        }
    }

    public static String getFilenameWithoutExtension(String filepath) {
        return getFilenameWithoutExtension(filepath, true);
    }

    public static String getFilenameWithoutExtension(String filepath, boolean removePath) {
        if (removePath) {
            filepath = new File(filepath).getName();
        }

        int index = filepath.lastIndexOf(".");

        if (index > 0) {
            return filepath.substring(0, index);
        } else {
            return filepath;
        }
    }

    public static File getModulePath() {
        try {
            return new File(FileHelper.class.getProtectionDomain().getCodeSource().getLocation().getPath() + PATH_UP + PATH_UP + PATH_UP).getCanonicalFile();
        } catch (IOException ignored) {
            return null;
        }
    }
}
