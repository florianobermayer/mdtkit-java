package com.mdtkit.java.util;

import junit.framework.AssertionFailedError;

import java.util.ArrayList;
import java.util.List;

public class TestUtil {
    public static abstract class Action {
        public abstract void run() throws Exception;
    }
    public static class Assert {



        public static void expectExceptionInAggregateExceptionOrDirectly(Class exceptionClass,
                                                                         String expectedExceptionMsg,
                                                                         Action action, boolean compareMessages, boolean compareMsgStartsWith) throws Exception {
            try {
                expectExceptionInAggregateException(exceptionClass, expectedExceptionMsg, action, compareMessages,
                        compareMsgStartsWith);
            } catch (AssertionFailedError e) {
                if (e.getMessage().startsWith("Thrown exception '") && e.getMessage().endsWith("' is no AggregateException!")) {
                    expectException(exceptionClass, expectedExceptionMsg, action, compareMessages,
                            compareMsgStartsWith);
                } else {
                    throw e;
                }
            }
        }

        public static void expectExceptionInAggregateException(Class exceptionClass, String expectedExceptionMsg,
                                                               Action action, boolean compareMessages, boolean compareMsgStartsWith) {
            boolean properlyCatched = false;
            try {
                action.run();
            } catch (AggregateException e) {
                List<Throwable> exceptions = e.getAllExceptions();

                if (exceptions == null || exceptions.size() == 0) {
                    throw new AssertionFailedError("AggregateException has no inner exceptions!");
                }

                for (Throwable ex : exceptions) {
                    if (!ex.getClass().equals(exceptionClass)) {
                        continue;
                    }

                    if (!compareMessages) {
                        properlyCatched = true;
                        break;
                    }

                    if (compareMsgStartsWith) {
                        properlyCatched = ex.getMessage().startsWith(expectedExceptionMsg);
                    } else {
                        properlyCatched = expectedExceptionMsg.equals(ex.getMessage());
                    }

                    if (properlyCatched) {
                        break;
                    }
                }
            } catch (Exception e) {
                throw new AssertionFailedError("Thrown exception '" + e.getClass().getSimpleName() + "' is no AggregateException!");
            }
            if (!properlyCatched) {
                if (expectedExceptionMsg != null && compareMsgStartsWith) {
                    expectedExceptionMsg += "...";
                }
                throw new AssertionFailedError(
                        "Expected Exception '" + exceptionClass.getSimpleName() + (expectedExceptionMsg == null ? "" : "(\"" + expectedExceptionMsg + "\")") + "' not thrown!");
            }
        }

        public static void expectException(Class exceptionType, String expectedExceptionMsg, Action action,
                                           boolean compareMessages, boolean compareMsgStartsWith) {
            boolean properlyCatched = false;
            try {
                action.run();
            } catch (Exception e) {
                org.junit.Assert.assertEquals(exceptionType, e.getClass());
                if (compareMessages) {
                    if (compareMsgStartsWith) {
                        org.junit.Assert.assertTrue("'" + e.getMessage() + "' does not start with: '" + expectedExceptionMsg + "'",
                                e.getMessage().startsWith(expectedExceptionMsg));
                    } else {
                        org.junit.Assert.assertEquals(expectedExceptionMsg, e.getMessage());
                    }
                }
                properlyCatched = true;
            }
            if (!properlyCatched) {
                if (expectedExceptionMsg != null && compareMsgStartsWith) {
                    expectedExceptionMsg += "...";
                }
                throw new AssertionFailedError(
                        "Expected Exception '" + exceptionType.getSimpleName() + (expectedExceptionMsg == null ? "" : "(\"" + expectedExceptionMsg + "\")") + "' not thrown!");
            }
        }

        public static void expectException(Class exceptionType, Action action) {
            expectException(exceptionType, null, action, false, false);
        }

        public static void expectAnyOfExceptions(List<Class> exceptionTypes, Action action, boolean includeDerived) {

            List<String> exceptionNames = new ArrayList<String>();
            for (Class exceptionClass : exceptionTypes) {
                exceptionNames.add(exceptionClass.getSimpleName());
            }

            String exceptions = Util.Join(", ", exceptionNames);
            boolean properlyCatched = false;
            try {
                action.run();
            } catch (Exception e) {
                boolean properException = false;

                if (includeDerived) {
                    for (Class exceptionClass : exceptionTypes) {
                        if (Util.isSameOrSubclass(exceptionClass, e.getClass())) {
                            properException = true;
                            break;
                        }
                    }

                    // exclude Assert Exceptions unless they where explicitly listed
                    if ((e.getClass().equals(AssertionFailedError.class) ||
                            e.getClass().equals(AssertionError.class)) && !exceptionTypes.contains(e.getClass())) {
                        properException = false;
                    }
                } else {
                    properException = exceptionTypes.contains(e.getClass());
                }

                if (!properException) {
                    throw new AssertionFailedError("Thrown exception '" + e.getClass().getSimpleName() + "' not in list of expected exceptions [" + exceptions + "]!");
                }
                properlyCatched = true;
            }
            if (!properlyCatched) {
                throw new AssertionFailedError("No expected exception [" + exceptions + "] was thrown!");
            }
        }

        public static void expectExceptionWithInnerException(Class innerExceptionType, Action action) {
            expectExceptionWithInnerException(innerExceptionType, null, action, false, false);
        }

        public static void expectExceptionWithInnerException(Class innerExceptionType, String innerExceptionMessage,
                                                             Action action, boolean compareMessages, boolean compareMsgStartsWith) {
            boolean properlyCatched = false;
            try {
                action.run();
            } catch (Exception e) {
                if (e.getCause() == null) {
                    throw new AssertionFailedError(
                            "InnerException of exception '" + e.getClass().getName() + "' is null, expected InnerException of Class '" + innerExceptionType.getName() + "'");
                }
                org.junit.Assert.assertEquals(e.getCause().getClass(), innerExceptionType);
                if (compareMessages) {
                    if (compareMsgStartsWith) {
                        org.junit.Assert.assertTrue("'" + e.getCause().getMessage() + "' does not start with: '" + innerExceptionMessage + "'", e.getCause().getMessage().startsWith(innerExceptionMessage));
                    } else {
                        org.junit.Assert.assertEquals(innerExceptionMessage, e.getCause().getMessage());
                    }
                }
                properlyCatched = true;
            }
            if (!properlyCatched) {
                if (innerExceptionMessage != null && compareMsgStartsWith) {
                    innerExceptionMessage += "...";
                }
                throw new AssertionFailedError("Expected InnerException '" + innerExceptionType.getName() + (innerExceptionMessage == null ? "" : "(\"" + innerExceptionMessage + "\")") + "' not thrown!");
            }
        }

        public static void expectExceptionOrDerived(Class exceptionType, Action action) {
            boolean properlyCatched = false;
            try {
                action.run();
            } catch (Exception e) {
                if (!Util.isSameOrSubclass(exceptionType, e.getClass())) {
                    throw new AssertionFailedError("Thrown exception '" + e.getClass().getName() + "' not Class of or subclass of expected exception '" + exceptionType.getName() + "'!");
                }

                org.junit.Assert.assertTrue(Util.isSameOrSubclass(exceptionType, e.getClass()));
                properlyCatched = true;
            }
            if (!properlyCatched) {
                throw new AssertionFailedError("Expected Exception '" + exceptionType.getName() + "' not thrown!");
            }
        }
    }
}
