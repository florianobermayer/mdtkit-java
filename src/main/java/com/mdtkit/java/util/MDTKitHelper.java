package com.mdtkit.java.util;

import com.mdtkit.java.MDTestCaseRunner;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class MDTKitHelper {

    public static MDTestCaseRunner getVerboseRunner() {
        return new MDTestCaseRunner() {
            @Override
            public void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) {
                String undefinedTypeName = "undefined";
                String tablefmt = "   |";

                List<String> headings = new ArrayList<String>();
                for (Map.Entry<String, Object> entry : row.entrySet()) {
                    Object object = entry.getValue();
                    String typename = null;
                    if (object != null) {
                        typename = entry.getValue().getClass().getSimpleName();
                    }

                    if (typename == null) {
                        typename = undefinedTypeName;
                    }

                    if (typename.equals(int.class.getSimpleName()) ||
                            typename.equals(double.class.getSimpleName()) ||
                            typename.equals(Integer.class.getSimpleName()) ||
                            typename.equals(Double.class.getSimpleName())) {
                        typename = "number";
                    } else if (typename.equals(Date.class.getSimpleName())) {
                        typename = "date";
                    }

                    typename = typename.toLowerCase();
                    headings.add(entry.getKey() + ":" + typename);
                }

                for (int i = 0; i < headings.size(); i++) {
                    tablefmt += " {" + i + "}\t\t|";
                }

                if (index == 0) {
                    String fmt = "   {0} \t{1}";
                    System.out.println("\nPre-Constants:");
                    for (Map.Entry<String, Object> entry : preConstants.entrySet()) {
                        System.out.println(MessageFormat.format(fmt, entry.getKey(), entry.getValue()));
                    }

                    System.out.println("\nPost-Constants:");
                    for (Map.Entry<String, Object> entry : postConstants.entrySet()) {
                        System.out.println(MessageFormat.format(fmt, entry.getKey(), entry.getValue()));
                    }

                    System.out.println("\nTable:");
                    String heading = MessageFormat.format(tablefmt, headings.toArray());
                    System.out.println(heading); // headings

                    String delimiterLine = "";
                    for (int i = 0; i < heading.length(); i++) {
                        delimiterLine += "-";
                    }
                    System.out.println("   |" + delimiterLine + "|");
                }

                List<String> valueStrings = new ArrayList<String>();
                for (Object obj : row.values()) {
                    if (obj != null) {
                        String objStr = obj.toString().replace("\t", "\\t").replace("\r\n", "\\n").replace("\n", "\\n");
                        valueStrings.add(objStr);
                    } else {
                        valueStrings.add("null");
                    }
                }

                System.out.println(MessageFormat.format(tablefmt, valueStrings.toArray()));
            }
        };
    }
}
