package com.mdtkit.java;

import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.MDExceptions.MDLinterStackException;
import com.mdtkit.java.MDExceptions.MDParserException;
import com.mdtkit.java.util.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.regex.Pattern;

public class MDTestData {

    static String TestDataFolderName = "testdata";
    private static String testDataBasePath;
    private MDTestTable testTable;
    private MDMap preConstants;
    private MDMap postConstants;

    private MDTestData(MDTestTable testTable, MDMap preConstants, MDMap postConstants) {
        this.testTable = testTable;
        this.preConstants = preConstants;
        this.postConstants = postConstants;
    }

    protected static String getTestDataBasePath() {
        if (testDataBasePath == null) {
            return FileHelper.getModulePath() + File.separator + TestDataFolderName; //TODO: confirm this location
        }
        return testDataBasePath;
    }

    public static void setTestDataBasePath(String testDataBasePath) {
        MDTestData.testDataBasePath = testDataBasePath;
    }

    public static MDTestData create(String testFilename, String testName) throws MDParserException, MDLinterException, AggregateException {
        if (!testFilename.endsWith(".md")) {
            testFilename += ".md";
        }
        String filenameWithoutExtension = FileHelper.getFilenameWithoutExtension(testFilename);
        List<File> results = FileHelper.getFileSystemEntries(new File(getTestDataBasePath()), Pattern.compile(".*" + filenameWithoutExtension + "\\.md"));


        if (results.size() > 1) {
            throw new MDParserException("multiple files found: '" + testFilename + "'");
        }

        if (results.size() == 1) {
            File file = results.get(0);
            return create(file, testName);
        }

        throw new MDParserException("No file found: '" + testFilename + "'");
    }

    public static MDTestData create(String testName, Class<? extends Object> testClass) throws MDParserException, MDLinterException, AggregateException {
        // if test class represents a single test method, this allows to simply name the testName
        String filename = testClass == null ? testName : testClass.getSimpleName();
        return create(filename, testName);
    }

    public static MDTestData create(File file, String testName) throws MDParserException, MDLinterException, AggregateException {

        String parserResult = getParsingResult(file);
        Map<String, Object> result;
        try {
            result = new JsonParser().readDictionary(parserResult);
        } catch (ParserException e) {
            throw new MDParserException("Could not parse result of module 'mdtkit-parser'", e);
        }

        raiseLinterErrors((Map<String, Object>) result.get("linter"));

        List<Map<String, Object>> testCases = (List<Map<String, Object>>) result.get("tests");
        Map<String, Object> testCase = null;

        for (Map<String, Object> currentTestCase :
                testCases) {
            if (currentTestCase.get("title").equals(testName)) {
                testCase = currentTestCase;
                break;
            }
        }

        if(testCase == null){
            throw new MDParserException("Cannot find '" + testName + "' in file '" + file.getName() + "'!");
        }

        return new MDTestData(extractTestTable(testCase), extractPrePostConstants(testCase, true),
                extractPrePostConstants(testCase, false));
    }

    private static MDMap extractPrePostConstants(Map<String, Object> testCase, boolean isPre) {
        MDMap result = new MDMap();
        List<Map<String, Object>> constants = (List<Map<String, Object>>) (isPre ? testCase.get("preConstants") : testCase.get("postConstants"));
        for (Map<String, Object> constant : constants) {
            result.put((String) constant.get("name"), castToCorrectType(constant, true));
        }

        return result;
    }

    private static MDTestTable extractTestTable(Map<String, Object> testCase) {


        List<String> headings = new ArrayList<String>();
        Map<String, Object> table = (Map<String, Object>) testCase.get("table");
        List<Map<String, Object>> tableHeader = (List<Map<String, Object>>) table.get("header");

        for (Map<String, Object> header: tableHeader){
            headings.add(((Map<String, String>)header.get("value")).get("title"));
        }

        List<MDMap> content = new ArrayList<MDMap>();

        List<List<Map<String, Object>>> contents = (List<List<Map<String, Object>>>) table.get("content");

        for(List<Map<String, Object>> row : contents)
        {
            MDMap rowMap = new MDMap();
            for (int i = 0; i < headings.size(); i++)
            {
                rowMap.put(headings.get(i), castToCorrectType(row.get(i), false));
            }
            content.add(rowMap);
        }
        return new MDTestTable(headings, content);
    }

    private static Object castToCorrectType(Map<String, Object> entry, boolean isPrePost) {
        String type = (String) entry.get("validType");
        Object value = isPrePost ? ((Map<String, Object>) entry.get("expression")).get("value") : entry.get("value");
        String stringValue = null;
        if(value != null){
            stringValue = value.toString();
        }

        if (type.equals("boolean")) {
            try {
                return Boolean.parseBoolean(stringValue);
            } catch (Exception e) {
                return false;
            }
        } else if (type.equals("number")) {
            if(stringValue == null){
                return 0;
            }
            return value;
        } else if (type.equals("date")) {
            try {
                return javax.xml.bind.DatatypeConverter.parseDateTime(stringValue).getTime();

            } catch (Exception e) {
                return (Date) null;
            }
        } else {
            return (String) value;
        }
    }

    public static String getMdtkitMainFilePath() throws MDParserException {
        return Util.Join(File.separator, PropertyLoader.getMDTKitModuleWorkingDir(), "src", "mdtkit-parser-cli.js");
    }


    private static void raiseLinterErrors(Map<String, Object> linter) throws MDLinterException, AggregateException {
        List<MDLinterException> exceptions = new Vector<MDLinterException>();
        List<Map<String, Object>> linterErrors = (List<Map<String, Object>>) linter.get("errors");
        List<Map<String, Object>> linterStack = (List<Map<String, Object>>) linter.get("stack");


        for (Map<String, Object> linterError : linterErrors) {
            exceptions.add(new MDLinterException((String) linterError.get("message"), Location.parse(linterError.get("location")),
                    (String) linterError.get("path")));
        }

        for (Map<String, Object> stackElem : linterStack) {
            exceptions.add(new MDLinterStackException((String) stackElem.get("blockName"), Location.parse(stackElem.get("location")),
                    (String) stackElem.get("path")));
        }

        if (exceptions.size() == 1) {
            throw exceptions.get(0);
        }

        if (exceptions.size() > 0) {
            throw new AggregateException((List<Exception>) (List<?>) exceptions);
        }
    }

    private static int getExitCode(ProcessBuilder processBuilder, StringBuilder inputStringBuilder, StringBuilder errorStringBuilder) {
        int exitCode = -1;
        Process p;
        BufferedReader input;
        BufferedReader errInput;
        String line;
        try {
            p = processBuilder.start();
            input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            errInput = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            if(inputStringBuilder != null){
                while ((line = input.readLine()) != null)
                    inputStringBuilder.append(line).append("\n");
            }

            if(errorStringBuilder != null){
                while ((line = errInput.readLine()) != null)
                    errorStringBuilder.append(line).append("\n");
            }

            input.close();
            exitCode = p.waitFor();
        } catch (IOException ignored) {
            ignored.toString();
        } catch(InterruptedException ignored) {
            ignored.toString();
        }
        return exitCode;
    }

    private static String getParsingResult(File file) throws MDParserException {
        String nodePath = PropertyLoader.getNodePath();
        String mdtkitMain = getMdtkitMainFilePath();
        String testFile = file.getAbsolutePath();
        File workingDir = new File(PropertyLoader.getMDTKitModuleWorkingDir());

        StringBuilder inputStringBuilder = new StringBuilder();
        StringBuilder errorStringBuilder = new StringBuilder();
        int exitCode = getExitCode(new ProcessBuilder(nodePath, mdtkitMain, testFile).directory(workingDir), inputStringBuilder, errorStringBuilder);

        if (exitCode != 0) {
            tryInitNodeModule();
            inputStringBuilder = new StringBuilder();
            errorStringBuilder = new StringBuilder();
            exitCode = getExitCode(new ProcessBuilder(nodePath, mdtkitMain, testFile).directory(workingDir), inputStringBuilder, errorStringBuilder);
            if(exitCode != 0){
                throw new MDParserException("Calling node module 'mdtkit-parser' failed. ExitCode: " + exitCode + " (" + errorStringBuilder.toString() + ")");
            }
        }

        return inputStringBuilder.toString();
    }

    private static void tryInitNodeModule() throws MDParserException {
        File workingDir = new File(PropertyLoader.getMDTKitModuleWorkingDir());
        System.out.println("Initializing node module mdtkit-parser (updating node modules)...");
        getExitCode(new ProcessBuilder(PropertyLoader.getNPMPath(), "update").directory(workingDir), null, null);
    }

    public MDTestTable getTestTable() {
        return testTable;
    }

    public MDMap getPreConstants() {
        return preConstants;
    }


    public MDMap getPostConstants() {
        return postConstants;
    }
}
