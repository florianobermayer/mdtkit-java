package com.mdtkit.java.MDExceptions;

public class MDParserException extends Exception {

    public MDParserException(String description) {
        super(description);
    }

    public MDParserException(String description, Exception e) {
        super(description, e);
    }
}
