package com.mdtkit.java.MDExceptions;

import com.mdtkit.java.util.Location;

public class MDLinterStackException extends MDLinterException {

    public MDLinterStackException(String message, Location location, String path)
    {
        super(formatBlockMessage(message), location, path);
    }

    private static String formatBlockMessage(String codeblockMessage)
    {
        int index = codeblockMessage.indexOf("_");
        if (index != -1)
        {
            return "Malformatted area: '" + codeblockMessage.substring(0, index) + "'";
        }
        return "Malformatted area: 'Unknown area'";
    }
}
