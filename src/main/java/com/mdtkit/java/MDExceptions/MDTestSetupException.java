package com.mdtkit.java.MDExceptions;

public class MDTestSetupException extends AbstractMDTestException {
    private static String Msg = "TEST SETUP FAILED!";

    public MDTestSetupException(String message, Exception exception, int rowIndex) {
        super(Msg, message, exception, rowIndex);
    }
    public MDTestSetupException(String message, Exception exception) {
        super(Msg, message, exception, -1);
    }

    public MDTestSetupException(Exception exception, int rowIndex) {
        super(Msg, exception, rowIndex);
    }
    public MDTestSetupException(Exception exception) {
        super(Msg, exception, -1);
    }

    public MDTestSetupException(String message, int rowIndex) {
        super(Msg, message, rowIndex);
    }
    public MDTestSetupException(String message) {
        super(Msg, message, -1);
    }
}
