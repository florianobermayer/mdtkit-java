package com.mdtkit.java.MDExceptions;

public class MDTestExcecutionException extends AbstractMDTestException {
    private static String Msg = "TEST EXCECUTION FAILED!";

    public MDTestExcecutionException(String message, Exception exception, int rowIndex) {
        super(Msg, message, exception, rowIndex);
    }
    public MDTestExcecutionException(String message, Exception exception) {
        super(Msg, message, exception, -1);
    }

    public MDTestExcecutionException(Exception exception, int rowIndex) {
        super(Msg, exception, rowIndex);
    }
    public MDTestExcecutionException(Exception exception) {
        super(Msg, exception, -1);
    }

    public MDTestExcecutionException(String message, int rowIndex) {
        super(Msg, message, rowIndex);
    }
    public MDTestExcecutionException(String message) {
        super(Msg, message, -1);
    }
}
