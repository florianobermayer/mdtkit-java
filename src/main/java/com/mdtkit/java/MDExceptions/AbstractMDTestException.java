package com.mdtkit.java.MDExceptions;


public abstract class AbstractMDTestException extends Exception{

    protected AbstractMDTestException(String title, String message, Exception exception, int rowIndex)
    {
        super(FormatWithRowIndex(title, rowIndex) + "[" + message + "]\n", exception);
    }

    protected AbstractMDTestException(String title, Exception exception, int rowIndex)
    {
        super(FormatWithRowIndex(title, rowIndex), exception);
    }

    protected AbstractMDTestException(String title, String message, int rowIndex)
    {
        super(FormatWithRowIndex(title, rowIndex) + "[" + message + "]\n");
    }

    private static String FormatWithRowIndex(String title, int index)
    {
        title = title + "\n";
        return index != -1 ? "[" +index +"] " + title : title;
    }
}

