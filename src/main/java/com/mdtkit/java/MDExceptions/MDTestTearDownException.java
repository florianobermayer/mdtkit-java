package com.mdtkit.java.MDExceptions;

public class MDTestTearDownException extends AbstractMDTestException {
    private static String Msg = "TEST TEAR DOWN FAILED!";

    public MDTestTearDownException(String message, Exception exception, int rowIndex) {
        super(Msg, message, exception, rowIndex);
    }
    public MDTestTearDownException(String message, Exception exception) {
        super(Msg, message, exception, -1);
    }

    public MDTestTearDownException(Exception exception, int rowIndex) {
        super(Msg, exception, rowIndex);
    }
    public MDTestTearDownException(Exception exception) {
        super(Msg, exception, -1);
    }

    public MDTestTearDownException(String message, int rowIndex) {
        super(Msg, message, rowIndex);
    }
    public MDTestTearDownException(String message) {
        super(Msg, message, -1);
    }
}
