package com.mdtkit.java.MDExceptions;

import com.mdtkit.java.util.Location;

public class MDLinterException extends Exception {

    private final Location location;
    private final String path;

    public MDLinterException(String message, Location location, String path) {
        super(message + "\n\nLocation:\n" + location + "\n\nPath:\n" + path);
        this.location = location;
        this.path = path;
    }

    public Location getLocation() {
        return location;
    }

    public String getPath() {
        return path;
    }
}

