package com.mdtkit.java;

import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.MDExceptions.MDParserException;
import com.mdtkit.java.util.*;

import junit.framework.AssertionFailedError;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class MDTKitTests extends MDTestCase {

    /**
     * region positive tests
     */

    @Test
    public void VerboseSmokeTest() throws MDParserException, MDLinterException, AggregateException {
        final MDMap preConst = new MDMap();
        final MDMap postConst = new MDMap();
        final MDMap preLinkedConst = new MDMap();
        final MDMap postLinkedConst = new MDMap();
        final MDMap preVar = new MDMap();
        final MDMap postVar = new MDMap();

        execute(new MDTestCaseRunner() {
            @Override
            public void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) {
                MDTKitHelper.getVerboseRunner().runOnEachRow(row, index, preConstants, postConstants);

                // const tests
                if (preConst.isEmpty() || postConst.isEmpty() || preVar.isEmpty() || postVar.isEmpty() || preLinkedConst.isEmpty() || postLinkedConst.isEmpty()) {
                    for (Map.Entry preEntry : preConstants.entrySet()) {
                        if (preEntry.getKey().toString().startsWith("c_")) {
                            preConst.put(preEntry.getKey().toString().replace("c_", ""), preEntry.getValue());
                        }
                    }

                    for (Map.Entry postEntry : postConstants.entrySet()) {
                        if (postEntry.getKey().toString().startsWith("c_")) {
                            postConst.put(postEntry.getKey().toString().replace("c_", ""), postEntry.getValue());
                        }
                    }

                    for (Map.Entry preEntry : preConstants.entrySet()) {
                        if (preEntry.getKey().toString().startsWith("cl_")) {
                            preLinkedConst.put(preEntry.getKey().toString().replace("cl_", ""), preEntry.getValue());
                        }
                    }

                    for (Map.Entry postEntry : postConstants.entrySet()) {
                        if (postEntry.getKey().toString().startsWith("cl_")) {
                            postLinkedConst.put(postEntry.getKey().toString().replace("cl_", ""), postEntry.getValue());
                        }
                    }

                    for (Map.Entry preEntry : preConstants.entrySet()) {
                        if (!preEntry.getKey().toString().startsWith("c_") && !preEntry.getKey().toString().startsWith("cl_")) {
                            preVar.put(preEntry.getKey().toString(), preEntry.getValue());
                        }
                    }

                    for (Map.Entry postEntry : postConstants.entrySet()) {
                        if (!postEntry.getKey().toString().startsWith("c_") && !postEntry.getKey().toString().startsWith("cl_")) {
                            postVar.put(postEntry.getKey().toString(), postEntry.getValue());
                        }
                    }

                    // test equality of const pre and post
                    for (Map.Entry prePair : preConst.entrySet()) {
                        assertEquals(prePair.getValue(), postConst.get(prePair.getKey()));
                    }

                    for (Map.Entry prePair : preLinkedConst.entrySet()) {
                        assertEquals(prePair.getValue(), postLinkedConst.get(prePair.getKey()));
                    }
                    // test for negative equality
                    assertEquals(preVar.getInt("int_nr"), -postVar.getInt("int_nr"));
                    assertEquals(preVar.getDouble("double_nr"), -postVar.getDouble("double_nr"), 0.00001);
                    assertEquals(preVar.getBoolean("bool"), !postVar.getBoolean("bool"));

                    // test for equality
                    assertEquals(preVar.getDate("date"), postVar.getDate("date"));
                    assertEquals(preVar.getString("string"), postVar.getString("string"));
                    assertEquals(preVar.getString("string_ml"), postVar.getString("string_ml"));
                    assertEquals(preVar.getString("undefined_string"), postVar.getString("undefined_string"));

                    assertNull(preVar.getString("undefined_string"));
                    assertNull(postVar.getString("undefined_string"));
                }

                //TODO: Table row tests
            }
        });
    }

    @Test
    public void DataTypes() throws MDParserException, MDLinterException, AggregateException {
        execute(MDTKitHelper.getVerboseRunner());
    }

    @Test
    public void ArithmeticExpressions() throws MDParserException, MDLinterException, AggregateException {
        execute(new MDTestCaseRunner() {
            @Override
            public void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) {
                MDTKitHelper.getVerboseRunner().runOnEachRow(row, index, preConstants, postConstants);

                assertEquals(row.get("booleanExpression"), row.get("booleanResult"));
                assertTrue((Double.valueOf(row.get("numberExpression").toString()) - Double.valueOf(row.get("numberResult").toString())) < 0.0000001);
            }
        });
    }

    @Test
    public void LinkedTables() throws MDParserException, MDLinterException, AggregateException {
        execute(new MDTestCaseRunner() {
            @Override
            public void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) {
                MDTKitHelper.getVerboseRunner().runOnEachRow(row, index, preConstants, postConstants);

                assertEquals(row.get("bool_exp"), row.get("bool_val"));
                assertEquals(row.get("number_exp"), row.get("number_val"));
            }
        });
    }

    @Test
    public void PathSeparators() throws MDParserException, MDLinterException, AggregateException {
        execute(new MDTestCaseRunner() {
            @Override
            public void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) {
                MDTKitHelper.getVerboseRunner().runOnEachRow(row, index, preConstants, postConstants);

                assertEquals(File.separator , preConstants.getString("separator"));

                try {
                    //noinspection ResultOfMethodCallIgnored
                    new File(row.getString("path")).getCanonicalFile();
                } catch (IOException e) {
                    throw new AssertionFailedError("'" + row.getString("path") + "' is no valid path");
                }
            }
        });
    }
}
