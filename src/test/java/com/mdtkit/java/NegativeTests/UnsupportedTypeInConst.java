package com.mdtkit.java.NegativeTests;

import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.MDTestCase;
import com.mdtkit.java.util.MDTKitHelper;
import com.mdtkit.java.util.TestUtil;
import com.mdtkit.java.util.TestUtil.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class UnsupportedTypeInConst extends MDTestCase {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void UnsupportedTypeInConstTest() throws Exception {

        Assert.expectExceptionInAggregateExceptionOrDirectly(
                MDLinterException.class,
                "Unsupported type: 'unsupported_type",
                new TestUtil.Action() {
                    @Override
                    public void run() throws Exception {
                        execute(MDTKitHelper.getVerboseRunner());
                    }
                },
                true,
                true);
    }
}
