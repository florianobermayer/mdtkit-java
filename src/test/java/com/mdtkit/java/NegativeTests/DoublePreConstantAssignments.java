package com.mdtkit.java.NegativeTests;

import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.MDExceptions.MDParserException;
import com.mdtkit.java.MDTestCase;
import com.mdtkit.java.util.AggregateException;
import com.mdtkit.java.util.MDTKitHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

public class DoublePreConstantAssignments extends MDTestCase {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void DoublePreConstantAssignmentsTest() throws MDParserException, MDLinterException, AggregateException {
        thrown.expect(MDLinterException.class);
        thrown.expectMessage(JUnitMatchers.containsString("Duplicate declaration of pre-constant 'double_assigned_var'"));
        execute(MDTKitHelper.getVerboseRunner());
    }
}
