package com.mdtkit.java.NegativeTests;

import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.MDExceptions.MDParserException;
import com.mdtkit.java.MDTestCase;
import com.mdtkit.java.util.AggregateException;
import com.mdtkit.java.util.MDTKitHelper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

public class NotProperlyFormattedStringInTable extends MDTestCase {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void NotProperlyFormattedStringInTableTest() throws MDParserException, MDLinterException, AggregateException {
        thrown.expect(MDLinterException.class);
        thrown.expectMessage(JUnitMatchers.containsString("Could not resolve arithmetic expression: 'I am mal-formatted because I'm not surrounded by matching quotes':\nSyntaxError: Unexpected identifier"));
        execute(MDTKitHelper.getVerboseRunner());
    }
    
}
