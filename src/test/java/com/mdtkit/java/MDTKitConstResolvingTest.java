package com.mdtkit.java;

import com.mdtkit.java.MDExceptions.MDLinterException;
import com.mdtkit.java.MDExceptions.MDParserException;
import com.mdtkit.java.util.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.nio.file.Paths;

public class MDTKitConstResolvingTest extends MDTestCase {

    @Test
    public void ConstantResolving() throws MDParserException, MDLinterException, AggregateException {
        execute("ConstantResolving", new MDTestCaseRunner() {
            @Override
            public void runOnEachRow(MDMap row, int index, MDMap preConstants, MDMap postConstants) {
                for (String key : preConstants.keySet()) {
                    assertEquals(preConstants.get(key), postConstants.get(key));
                }
            }
        });
    }

    @Test
    public void ConstantResolvingFail() throws Exception {

        TestUtil.Assert.expectExceptionInAggregateExceptionOrDirectly(MDLinterException.class,
                "Could not resolve value '$IIIIIUNUSED'",
                new TestUtil.Action() {
                    @Override
                    public void run() throws Exception {
                        execute("ConstantResolvingFail", "ConstantResolvingFail", MDTKitHelper.getVerboseRunner());
                    }
                },
                true,
                true);

    }
}
